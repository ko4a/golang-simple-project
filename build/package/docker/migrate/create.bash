#!/bin/bash

docker-compose run --rm -u 1000 migrate create -ext sql -dir ./migrations -seq "$1"