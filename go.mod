module gorest

go 1.16

require (
	github.com/c2fo/testify v0.0.0-20150827203832-fba96363964a // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/doug-martin/goqu v5.0.0+incompatible
	github.com/doug-martin/goqu/v9 v9.13.0
	github.com/gin-gonic/gin v1.7.2
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df // indirect
	github.com/go-playground/validator/v10 v10.6.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/jmoiron/sqlx v1.3.4
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.1
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/retailify/go-interval v0.0.0-20180427165929-297a169340a9 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/ugorji/go v1.2.6 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/sys v0.0.0-20210525143221-35b2ab0089ea // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/doug-martin/goqu.v5 v5.0.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
