DC=docker-compose
DCEXEC=${DC} exec
DCEXEC_GO=${DCEXEC} -u www-data go

.PHONY: up migrate migrate-create

install: up migrate

up:
	${DC} up -d --build --force-recreate

psql:
	${DCEXEC} db psql -U user -d restapp

migrate:
	./build/package/docker/migrate/migrate.bash

migrate-down:
	./build/package/docker/migrate/down.bash

migrate-create:
	./build/package/docker/migrate/create.bash $(name)