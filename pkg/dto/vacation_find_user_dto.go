package dto

type VacationFindByUserDTO struct {
	UserId int
	Page   int `form:"page" binding:"required"`
	Limit  int `form:"limit" binding:"required"`
}
