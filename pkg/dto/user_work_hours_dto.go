package dto

import "time"

type UserWorkHoursCalculationDTO struct {
	StartDate time.Time
	EndDate   time.Time
	UserId    int
}
