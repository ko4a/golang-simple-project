package dto

import (
	"gorest/pkg/entity"
)

type CorporateReadDTO struct {
	Id      int    `json:"id" db:"id"`
	Name    string `json:"name" db:"name"`
	StartAt string `json:"start_at" db:"start_at"`
	EndAt   string `json:"end_at" db:"end_at"`
}

func (c *CorporateReadDTO) Assemble(corporate *entity.Corporate) {
	c.Id = corporate.Id
	c.Name = corporate.Name
	c.StartAt = corporate.StartAt.Format("2006-01-02 15:04:05")
	c.EndAt = corporate.EndAt.Format("2006-01-02 15:04:05")
}
