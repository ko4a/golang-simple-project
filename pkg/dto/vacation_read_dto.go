package dto

import "gorest/pkg/entity"

type VacationReadDTO struct {
	Id      int    `json:"id"`
	StartAt string `json:"start_at"`
	EndAt   string `json:"end_at"`
	UserId  int    `json:"user_id"`
}

func (v *VacationReadDTO) Assemble(vac *entity.Vacation) {
	v.Id = vac.Id
	v.StartAt = vac.StartAt.Format("2006-01-02 15:04:05")
	v.EndAt = vac.EndAt.Format("2006-01-02 15:04:05")
	v.UserId = vac.UserId
}
