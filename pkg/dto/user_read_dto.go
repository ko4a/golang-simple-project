package dto

import (
	"gorest/pkg/entity"
)

type UserReadDTO struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Username string `json:"username"`
	Email    string `json:"email"`
	GotJobAt string `json:"got_job_at"`
}

func (u *UserReadDTO) Assemble(user *entity.User) {
	u.Id = user.Id
	u.Name = user.Name
	u.Username = user.Username
	u.Email = user.Email
	u.GotJobAt = user.GotJobAt.Format("2006-01-02 15:04:05")
}
