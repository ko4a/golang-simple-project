package dto

type CorporateFindByUserDTO struct {
	UserID int
	Page   int `form:"page" binding:"required"`
	Limit  int `form:"limit" binding:"required"`
}
