package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"gorest/pkg/dto"
	"gorest/pkg/entity"
	"net/http"
	"strconv"
	"time"
)

type vacationInput struct {
	StartAt string `json:"start_at" binding:"required"`
	EndAt   string `json:"end_at" binding:"required"`
}

func (h *Handler) getUserVacations(c *gin.Context) {
	userId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "id parse from arg error")
		return
	}

	var filter dto.VacationFindByUserDTO

	if err = c.BindQuery(&filter); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	filter.UserId = userId

	h.services.Vacation.FindByUser(&filter)

	c.JSON(http.StatusOK, userId)
}

func (h *Handler) createUserVacation(c *gin.Context) {
	userId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "id parse from arg error")
		return
	}

	var input vacationInput

	if err = c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	start, err := time.Parse("2006-01-02 15:04:05", input.StartAt)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	end, err := time.Parse("2006-01-02 15:04:05", input.EndAt)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	if end.Before(start) {
		newErrorResponse(c, http.StatusBadRequest, "start date should be before end date")
		return
	}

	vac, err := h.services.Vacation.CreateVacation(&entity.Vacation{
		UserId:  userId,
		StartAt: start,
		EndAt:   end,
	})

	res := dto.VacationReadDTO{}
	res.Assemble(vac)

	c.JSON(http.StatusOK, res)
}

func (h *Handler) getUserCorporates(c *gin.Context) {
	var filter dto.CorporateFindByUserDTO

	if err := c.BindQuery(&filter); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	if filter.Page <= 0 || filter.Limit <= 0 {
		newErrorResponse(c, http.StatusBadRequest, "page and limit should be bigger than 0")
		return
	}

	var userId, err = strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "id parsing error")
		return
	}

	filter.UserID = userId

	corporates, err := h.services.Corporate.FindByUser(&filter)

	if err != nil {
		logrus.Errorf("user corporates request failed %s", err.Error())
		newErrorResponse(c, http.StatusBadRequest, "Bad request")
	}

	c.JSON(http.StatusOK, corporates)
}

func (h *Handler) signUserToCorporate(c *gin.Context) {
	userId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "id parse from arg error")
		return
	}

	corporateId, err := strconv.Atoi(c.Param("corporate_id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "corporate id parse from arg error")
		return
	}

	err = h.services.Corporate.AddUser(corporateId, userId)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "Cant sign user to corporate")
		return
	}
	c.JSON(http.StatusOK, map[string]interface{}{
		"corporate_id": corporateId,
		"user_id":      userId,
	})

}

func (h *Handler) getUser(c *gin.Context) {
	userId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "id parse from arg error")
		return
	}

	user, err := h.services.User.GetUser(userId)

	c.JSON(http.StatusOK, user)
}
