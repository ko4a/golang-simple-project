package handler

import (
	"github.com/gin-gonic/gin"
	"gorest/pkg/dto"
	"gorest/pkg/entity"
	"net/http"
	"time"
)

type corporateInput struct {
	Name    string `json:"name" binding:"required"`
	StartAt string `json:"start_at" binding:"required"`
	EndAt   string `json:"end_at" binding:"required"`
}

func (h *Handler) createCorporate(c *gin.Context) {
	var input corporateInput

	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	start, err := time.Parse("2006-01-02 15:04:05", input.StartAt)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	end, err := time.Parse("2006-01-02 15:04:05", input.EndAt)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	if end.Before(start) {
		newErrorResponse(c, http.StatusBadRequest, "start date should be before end date")
		return
	}

	corp, err := h.services.Corporate.CreateCorporate(&entity.Corporate{
		Name:    input.Name,
		StartAt: start,
		EndAt:   end,
	})

	res := dto.CorporateReadDTO{}
	res.Assemble(corp)

	c.JSON(http.StatusOK, res)
}
