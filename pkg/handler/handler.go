package handler

import (
	"github.com/gin-gonic/gin"
	"gorest/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	api := router.Group("/api")
	{
		security := api.Group("/security")
		{
			security.POST("/signup", h.signUp)
			security.POST("/login", h.login)
		}

		user := api.Group("/user", h.userIdentity)
		{
			user.GET("/:id", h.getUser)
			user.GET("/:id/schedule", h.schedule)
			user.GET("/:id/corporate", h.getUserCorporates)
			user.GET("/:id/vacation", h.getUserVacations)
			user.POST("/:id/corporate/:corporate_id", h.signUserToCorporate)
			user.POST("/:id/vacation", h.createUserVacation)
		}

		corporateParty := api.Group("/corporate")
		{
			corporateParty.POST("", h.createCorporate)
		}

	}

	return router
}
