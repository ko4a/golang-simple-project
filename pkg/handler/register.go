package handler

import (
	"github.com/gin-gonic/gin"
	"gorest/pkg/dto"
	"gorest/pkg/entity"
	"gorest/pkg/service"
	"net/http"
	"time"
)

type userCreateInput struct {
	Name     string `json:"name" binding:"required"`
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Email    string `json:"email" binding:"required"`
	GotJobAt string `json:"got_job_at" binding:"required"`
}

func (h *Handler) signUp(c *gin.Context) {
	var userInput userCreateInput

	if err := c.BindJSON(&userInput); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	gotJobAt, err := time.Parse("2006-01-02 15:04:05", userInput.GotJobAt)

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	user, err := h.services.Registration.Register(&entity.User{
		Name:     userInput.Name,
		Username: userInput.Username,
		Password: userInput.Password,
		Email:    userInput.Email,
		GotJobAt: gotJobAt,
	})

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	res := dto.UserReadDTO{}
	res.Assemble(user)

	c.JSON(http.StatusOK, res)

	go func() {
		email := &service.Email{
			ReceiverEmail: userInput.Email,
			Body:          "Congratulations with registration!",
			Subject:       "Congratulations with registration!",
		}
		_ = h.services.Mailer.Send(email)
	}()

}
