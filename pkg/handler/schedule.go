package handler

import (
	"github.com/gin-gonic/gin"
	"gorest/pkg/dto"
	"net/http"
	"strconv"
	"time"
)

type UserWorkHoursCalculationInput struct {
	StartDate string `form:"start_date" binding:"required"`
	EndDate   string `form:"end_date" binding:"required"`
	UserId    int
}

func (h *Handler) schedule(c *gin.Context) {
	var input UserWorkHoursCalculationInput

	if err := c.ShouldBindQuery(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	start, err := time.Parse("2006-01-02", input.StartDate)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	end, err := time.Parse("2006-01-02", input.EndDate)
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	if end.Before(start) {
		newErrorResponse(c, http.StatusBadRequest, "start date should be before end date")
	}

	userId, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	_, _ = h.services.Schedule.CalculateUserWorkHours(&dto.UserWorkHoursCalculationDTO{
		StartDate: start,
		EndDate:   end,
		UserId:    userId,
	})


	c.JSON(http.StatusOK, map[string]interface{}{
		"id": 1,
	})

}
