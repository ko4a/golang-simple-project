package service

import (
	"gorest/pkg/dto"
	"gorest/pkg/entity"
	"gorest/pkg/repository"
)

type Registration interface {
	Register(user *entity.User) (*entity.User, error)
	GeneratePasswordHash(plain string) (string, error)
}

type Authentication interface {
	Auth(username, plainPassword string) (string, error)
	ParseToken(token string) (*entity.User, error)
}

type User interface {
	GetUser(id int) (*dto.UserReadDTO, error)
	GetUserCorporates(filter *dto.CorporateFindByUserDTO) ([]dto.CorporateReadDTO, error)
}

type Mailer interface {
	Send(email *Email) error
}

type Corporate interface {
	CreateCorporate(corporate *entity.Corporate) (*entity.Corporate, error)
	AddUser(corporateId, userId int) error
	FindByUser(filter *dto.CorporateFindByUserDTO) ([]dto.CorporateReadDTO, error)
}

type Vacation interface {
	CreateVacation(vac *entity.Vacation) (*entity.Vacation, error)
	FindByUser(filter *dto.VacationFindByUserDTO) ([]dto.VacationReadDTO, error)
}

type Schedule interface {
	CalculateUserWorkHours(dto *dto.UserWorkHoursCalculationDTO) (int, error)
}

type Service struct {
	Registration
	Authentication
	User
	Mailer
	Corporate
	Vacation
	Schedule
}

func NewService(repos *repository.Repository) *Service {
	register := NewRegisterService(repos)

	return &Service{
		Registration:   register,
		Authentication: NewAuthenticationService(repos),
		User:           NewUserService(repos),
		Mailer:         NewMailerService(repos),
		Corporate:      NewCorporateService(repos),
		Vacation:       NewVacationService(repos),
		Schedule:		NewScheduleService(repos),
	}
}
