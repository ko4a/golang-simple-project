package service

import (
	"gorest/pkg/dto"
	"gorest/pkg/entity"
	"gorest/pkg/repository"
)

type VacationService struct {
	repos *repository.Repository
}

func NewVacationService(repos *repository.Repository) *VacationService {
	return &VacationService{repos: repos}
}

func (v *VacationService) FindByUser(filter *dto.VacationFindByUserDTO) ([]dto.VacationReadDTO, error) {
	return v.repos.Vacation.FindByUser(filter)
}

func (v *VacationService) CreateVacation(corporate *entity.Vacation) (*entity.Vacation, error) {
	return v.repos.Vacation.CreateVacation(corporate)
}
