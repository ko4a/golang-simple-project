package service

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"
	"gorest/pkg/entity"
	"gorest/pkg/repository"
	"os"
	"time"
)

const (
	tokenTTL = 12 * time.Hour
)

type tokenClaims struct {
	jwt.StandardClaims
	UserId int `json:"user_id"`
}

type AuthenticationService struct {
	repo *repository.Repository
}

func NewAuthenticationService(repo *repository.Repository) *AuthenticationService {
	return &AuthenticationService{
		repo: repo,
	}
}

func (s *AuthenticationService) ParseToken(accessToken string) (*entity.User, error) {
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("Invlaid signing method")
		}

		return []byte(os.Getenv("JWT_KEY")), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*tokenClaims)
	if !ok {
		return nil, errors.New("token claims are not of type *tokenClaims")
	}

	return s.repo.User.FindOneBy("id", claims.UserId)
}

func (s *AuthenticationService) Auth(username, plainPassword string) (string, error) {
	user, err := s.repo.User.FindOneBy("username", username)

	if err != nil {
		return "", err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(plainPassword))

	if err != nil {
		return "", err
	}

	signingKey := os.Getenv("JWT_KEY")

	if signingKey == "" {
		logrus.Error("no JWT_KEY value in environment")
		return "", errors.New("no JWT_KEY value in environment")
	}

	now := time.Now()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			IssuedAt:  now.Unix(),
			ExpiresAt: now.Add(tokenTTL).Unix(),
		},
		user.Id,
	})

	return token.SignedString([]byte(signingKey))
}
