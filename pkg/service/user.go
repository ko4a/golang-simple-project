package service

import (
	"gorest/pkg/dto"
	"gorest/pkg/repository"
)

type UserService struct {
	repo *repository.Repository
}

func NewUserService(repo *repository.Repository) *UserService {
	return &UserService{repo: repo}
}

func (s *UserService) GetUserCorporates(filter *dto.CorporateFindByUserDTO) ([]dto.CorporateReadDTO, error) {
	return s.repo.Corporate.FindByUser(filter)
}

func (s *UserService) GetUser(id int) (*dto.UserReadDTO, error) {
	user, err := s.repo.FindOneBy("id", id)
	if err != nil {
		return nil, err
	}

	userReadDto := &dto.UserReadDTO{}
	userReadDto.Assemble(user)

	return userReadDto, nil
}
