package service

import (
	"gorest/pkg/dto"
	"gorest/pkg/entity"
	"gorest/pkg/repository"
)

type CorporateService struct {
	repos *repository.Repository
}

func NewCorporateService(repos *repository.Repository) *CorporateService {
	return &CorporateService{repos: repos}
}

func (c *CorporateService) FindByUser(filter *dto.CorporateFindByUserDTO) ([]dto.CorporateReadDTO, error) {
	return c.repos.Corporate.FindByUser(filter)
}
func (c *CorporateService) AddUser(corporateId, userId int) error {
	return c.repos.Corporate.AddUser(corporateId, userId)
}

func (c *CorporateService) CreateCorporate(corporate *entity.Corporate) (*entity.Corporate, error) {
	return c.repos.Corporate.CreateCorporate(corporate)
}
