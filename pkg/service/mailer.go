package service

import (
	"errors"
	"github.com/go-gomail/gomail"
	"github.com/sirupsen/logrus"
	"gorest/pkg/repository"
	"os"
	"strconv"
)

type Email struct {
	ReceiverEmail string
	Body          string
	Subject       string
}

type MailerService struct {
	repo *repository.Repository
}

func NewMailerService(repo *repository.Repository) *MailerService {
	return &MailerService{repo: repo}
}

func (m *MailerService) Send(email *Email) error {
	host := os.Getenv("SMTP_HOST")
	port, err := strconv.Atoi(os.Getenv("SMTP_PORT"))
	sender := os.Getenv("EMAIL_SENDER")
	senderPwd := os.Getenv("EMAIL_SENDER_PWD")

	if host == "" || err != nil || sender == "" || senderPwd == "" {
		logrus.Error("no env variables for smtp communication")
		return errors.New("no env variables for smtp communication")
	}

	message := gomail.NewMessage()
	message.SetHeader("From", sender)
	message.SetHeader("To", email.ReceiverEmail)
	message.SetHeader("Subject", email.Subject)
	message.SetBody("text/plain", email.Body)

	d := gomail.NewDialer(host, port, sender, senderPwd)

	if err := d.DialAndSend(message); err != nil {
		logrus.Errorf("Error during mail sending %s", err.Error())
		return err
	}

	return nil
}
