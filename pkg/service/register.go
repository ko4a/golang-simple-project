package service

import (
	"golang.org/x/crypto/bcrypt"
	"gorest/pkg/entity"
	"gorest/pkg/repository"
)

type RegisterService struct {
	repo *repository.Repository
}

func NewRegisterService(repo *repository.Repository) *RegisterService {
	return &RegisterService{repo: repo}
}

func (s *RegisterService) Register(user *entity.User) (*entity.User, error) {
	hashedPwd, err := s.GeneratePasswordHash(user.Password)

	if err != nil {
		return nil, err
	}

	user.Password = hashedPwd

	return s.repo.Registration.Register(user)
}

func (s *RegisterService) GeneratePasswordHash(plain string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(plain), bcrypt.MinCost)

	if err != nil {
		return "", err
	}

	return string(hash), nil
}
