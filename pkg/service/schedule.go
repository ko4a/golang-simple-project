package service

import (
	"encoding/xml"
	"errors"
	"fmt"
	"github.com/retailify/go-interval"
	"github.com/sirupsen/logrus"
	"gorest/pkg/dto"
	"gorest/pkg/repository"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
)

type ScheduleService struct {
	repo *repository.Repository
}

type XmlDay struct {
	Day  string `xml:"d,attr"`
	Type int    `xml:"t,attr"`
}

type XmlDays struct {
	Day []XmlDay `xml:"days>day"`
}

func NewScheduleService(repo *repository.Repository) *ScheduleService {
	return &ScheduleService{repo: repo}
}

func (s *ScheduleService) CalculateUserWorkHours(dto *dto.UserWorkHoursCalculationDTO) (int, error) {
	s.getUserNonWorkingTime(dto)

	return 0, nil
}

func (s *ScheduleService) getUserNonWorkingTime(dto *dto.UserWorkHoursCalculationDTO) ([]interval.TimeInterval, error) {
	nonWorkIntervals, err := s.repo.Schedule.FindUserNonWorkIntervals(dto)

	if len(nonWorkIntervals) == 0 {
		fmt.Println("0")
		return nonWorkIntervals, nil
	}

	minYear := nonWorkIntervals[0].Start().Year()
	maxYear := nonWorkIntervals[0].Start().Year()

	for _, v := range nonWorkIntervals {
		if v.Start().Year() < minYear {
			minYear = v.Start().Year()
		}
		if v.End().Year() > maxYear {
			maxYear = v.End().Year()
		}
	}

	if maxYear > time.Now().Year() {
		maxYear = time.Now().Year()
	}

	days, err := s.fetchHolidays(minYear, maxYear)

	for _, v := range days {
		fmt.Println(v.Day)
	}

	if err != nil {
		return nil, err
	}

	return nil, nil
}

func (s *ScheduleService) fetchHolidays(yearFrom, yearTo int) (map[int]*XmlDays, error) {
	var endpoint = os.Getenv("CALENDAR_API_ENDPOINT")

	if endpoint == "" {
		logrus.Error("env CALENDAR_API_ENDPOINT is not set")
		return nil, errors.New("env CALENDAR_API_ENDPOINT is not set")
	}

	result := make(map[int]*XmlDays)

	for i := yearFrom; i <= yearTo; i++ {
		resp, err := http.Get(fmt.Sprintf(endpoint, strconv.Itoa(i)))

		if err != nil {
			logrus.Errorf("Error during fetching calendar %s", err.Error())
			return nil, err
		}

		body, err := io.ReadAll(resp.Body)

		if err != nil {
			logrus.Errorf("Error during reading fetched calendar %s", err.Error())
			return nil, err
		}

		err = resp.Body.Close()

		if err != nil {
			logrus.Errorf("Error during close body http response %s", err.Error())
		}

		var days XmlDays

		err = xml.Unmarshal(body, &days)

		if err != nil {
			logrus.Errorf("error during xml parsing %s", err.Error())
			return nil, err
		}

		result[i] = &days
	}

	return result, nil
}
