package repository

import "github.com/doug-martin/goqu/v9"

func Paginate(data *goqu.SelectDataset, page, limit uint) *goqu.SelectDataset {
	return data.Limit(limit).Offset(limit * (page - 1))
}
