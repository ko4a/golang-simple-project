package repository

import (
	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/postgres"
	"github.com/jmoiron/sqlx"
	"github.com/retailify/go-interval"
	"github.com/sirupsen/logrus"
	"gorest/pkg/dto"
	"time"
)

type ScheduleRepository struct {
	db *sqlx.DB
}

func NewScheduleRepository(db *sqlx.DB) *ScheduleRepository {
	return &ScheduleRepository{
		db: db,
	}
}

type UserNonWorkIntervals struct {
	StartAt *time.Time `db:"start_at"`
	EndAt   *time.Time `db:"end_at"`
}

func (r *ScheduleRepository) FindUserNonWorkIntervals(filter *dto.UserWorkHoursCalculationDTO) ([]interval.TimeInterval, error) {
	dialect := goqu.Dialect("postgres")

	var queryResult []UserNonWorkIntervals

	corps := dialect.Select(
		corporateTable+".start_at",
		corporateTable+".end_at",
	).From(usersTable).Join(
		goqu.T(userCorporateTable),
		goqu.On(goqu.Ex{usersTable + ".id": goqu.I(userCorporateTable + ".user_profile_id")}),
	).Join(
		goqu.T(corporateTable),
		goqu.On(goqu.Ex{userCorporateTable + ".corporate_party_id": goqu.I(corporateTable + ".id")}),
	).Where(goqu.Ex{
		usersTable + ".id":           goqu.Op{"eq": filter.UserId},
		corporateTable + ".start_at": goqu.Op{"gte": filter.StartDate},
		corporateTable + ".end_at":   goqu.Op{"lte": filter.EndDate},
	}).Prepared(true)

	vacs := dialect.Select(
		vacationTable+".start_at",
		vacationTable+".end_at",
	).From(vacationTable).Where(goqu.Ex{
		vacationTable + ".user_profile_id": goqu.Op{"eq": filter.UserId},
		vacationTable + ".start_at":        goqu.Op{"gte": filter.StartDate},
		vacationTable + ".end_at":          goqu.Op{"lte": filter.EndDate},
	}).Prepared(true)

	vacs.Union(corps)

	sql, args, err := vacs.ToSQL()

	if err != nil {
		logrus.Errorf("error during FindUserNonWorkIntervals sql generation %s", err.Error())
		return nil, err
	}

	err = r.db.Select(&queryResult, sql, args...)

	if err != nil {
		logrus.Errorf("error during find by user vacation repository query execution %s", err.Error())
		return nil, err
	}

	res := make([]interval.TimeInterval, 0, len(queryResult))

	for _, v := range queryResult {
		timeInterval, err := interval.MakeTimeInterval(v.StartAt, v.EndAt)

		if err != nil || timeInterval == nil {
			logrus.Errorf("error during convert database intervals to go timeInterval %s", err.Error())
			return nil, err
		}
		res = append(res, *timeInterval)
	}

	return res, nil

}
