package repository

import (
	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/postgres"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gorest/pkg/dto"
	"gorest/pkg/entity"
)

type VacationRepository struct {
	db *sqlx.DB
}

func NewVacationRepository(db *sqlx.DB) *VacationRepository {
	return &VacationRepository{
		db: db,
	}
}

func (r *VacationRepository) FindByUser(filter *dto.VacationFindByUserDTO) ([]dto.VacationReadDTO, error) {
	dialect := goqu.Dialect("postgres")

	var corporates []dto.VacationReadDTO

	query := dialect.Select(
		vacationTable+".id",
		vacationTable+".start_at",
		vacationTable+".end_at",
	).From(vacationTable).Where(goqu.Ex{vacationTable + ".user_profile_id": filter.UserId}).Prepared(true)

	query = Paginate(query, uint(filter.Page), uint(filter.Limit))

	sql, args, err := query.ToSQL()

	if err != nil {
		logrus.Errorf("error during findBy user vacation repository sql generation %s", err.Error())
		return nil, err
	}

	err = r.db.Select(&corporates, sql, args...)

	if err != nil {
		logrus.Errorf("error during find by user vacation repository query execution %s", err.Error())
		return nil, err
	}

	return corporates, err
}

func (r *VacationRepository) CreateVacation(vac *entity.Vacation) (*entity.Vacation, error) {
	dialect := goqu.Dialect("postgres")

	sql, args, err := dialect.Insert(vacationTable).Prepared(true).Rows(
		goqu.Record{
			"start_at":        vac.StartAt.Format("2006-01-02 15:04:05"),
			"end_at":          vac.EndAt.Format("2006-01-02 15:04:05"),
			"user_profile_id": vac.UserId,
		},
	).Returning("id").ToSQL()

	if err != nil {
		logrus.Errorf("error during sql corporate create generation: %s", err.Error())
		return nil, err
	}

	row := r.db.QueryRow(sql, args...)

	if err := row.Scan(&vac.Id); err != nil {
		logrus.Errorf("error during sql user register generation: %s", err.Error())
		return nil, err
	}

	return vac, nil
}
