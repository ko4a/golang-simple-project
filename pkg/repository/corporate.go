package repository

import (
	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/postgres"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gorest/pkg/dto"
	"gorest/pkg/entity"
)

type CorporateRepository struct {
	db *sqlx.DB
}

func NewCorporateRepository(db *sqlx.DB) *CorporateRepository {
	return &CorporateRepository{db: db}
}

func (r *CorporateRepository) FindByUser(filter *dto.CorporateFindByUserDTO) ([]dto.CorporateReadDTO, error) {
	dialect := goqu.Dialect("postgres")
	var corporates []dto.CorporateReadDTO

	query := dialect.Select(
		corporateTable+".id",
		corporateTable+".name",
		corporateTable+".start_at",
		corporateTable+".end_at",
	).From(usersTable).Join(
		goqu.T(userCorporateTable),
		goqu.On(goqu.Ex{usersTable + ".id": goqu.I(userCorporateTable + ".user_profile_id")}),
	).Join(
		goqu.T(corporateTable),
		goqu.On(goqu.Ex{userCorporateTable + ".corporate_party_id": goqu.I(corporateTable + ".id")}),
	).Where(goqu.Ex{usersTable + ".id": filter.UserID}).Prepared(true)

	query = Paginate(query, uint(filter.Page), uint(filter.Limit))

	sql, args, err := query.ToSQL()

	if err != nil {
		logrus.Errorf("error during findBy user corporate repository sql generation %s", err.Error())
		return nil, err
	}

	err = r.db.Select(&corporates, sql, args...)

	if err != nil {
		logrus.Errorf("error during find by user corporate repository query execution %s", err.Error())
		return nil, err
	}

	return corporates, err

}

func (r *CorporateRepository) AddUser(corporateId, userId int) error {
	dialect := goqu.Dialect("postgres")

	sql, args, err := dialect.Insert(userCorporateTable).Prepared(true).Rows(
		goqu.Record{
			"user_profile_id":    userId,
			"corporate_party_id": corporateId,
		},
	).ToSQL()

	if err != nil {
		logrus.Errorf("error during sql user sign to corporate generation: %s", err.Error())
		return err
	}

	_, err = r.db.Exec(sql, args...)

	if err != nil {
		logrus.Errorf("error during query execution: %s", err.Error())
		return err
	}

	return nil
}

func (r *CorporateRepository) CreateCorporate(corporate *entity.Corporate) (*entity.Corporate, error) {
	dialect := goqu.Dialect("postgres")

	sql, args, err := dialect.Insert(corporateTable).Prepared(true).Rows(
		goqu.Record{
			"name":     corporate.Name,
			"start_at": corporate.StartAt.Format("2006-01-02 15:04:05"),
			"end_at":   corporate.EndAt.Format("2006-01-02 15:04:05"),
		},
	).Returning("id").ToSQL()

	if err != nil {
		logrus.Errorf("error during sql corporate create generation: %s", err.Error())
		return nil, err
	}

	row := r.db.QueryRow(sql, args...)

	if err := row.Scan(&corporate.Id); err != nil {
		logrus.Errorf("error during sql user register generation: %s", err.Error())
		return nil, err
	}

	return corporate, nil
}
