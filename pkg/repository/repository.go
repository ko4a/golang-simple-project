package repository

import (
	"github.com/jmoiron/sqlx"
	"github.com/retailify/go-interval"
	"gorest/pkg/dto"
	"gorest/pkg/entity"
)

type Registration interface {
	Register(user *entity.User) (*entity.User, error)
}

type User interface {
	FindOneBy(field string, value interface{}) (*entity.User, error)
}

type Corporate interface {
	CreateCorporate(corporate *entity.Corporate) (*entity.Corporate, error)
	AddUser(corporateId, userId int) error
	FindByUser(filter *dto.CorporateFindByUserDTO) ([]dto.CorporateReadDTO, error)
}

type Vacation interface {
	CreateVacation(vac *entity.Vacation) (*entity.Vacation, error)
	FindByUser(filter *dto.VacationFindByUserDTO) ([]dto.VacationReadDTO, error)
}

type Schedule interface {
	FindUserNonWorkIntervals(filter *dto.UserWorkHoursCalculationDTO) ([]interval.TimeInterval, error)
}

type Repository struct {
	Registration
	User
	Corporate
	Vacation
	Schedule
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Registration: NewRegisterRepository(db),
		User:         NewUserRepository(db),
		Corporate:    NewCorporateRepository(db),
		Vacation:     NewVacationRepository(db),
		Schedule:     NewScheduleRepository(db),
	}
}
