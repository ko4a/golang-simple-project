package repository

import (
	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/postgres"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gorest/pkg/entity"
)

type RegisterRepository struct {
	db *sqlx.DB
}

func NewRegisterRepository(db *sqlx.DB) *RegisterRepository {
	return &RegisterRepository{db: db}
}

func (r *RegisterRepository) Register(user *entity.User) (*entity.User, error) {
	dialect := goqu.Dialect("postgres")

	sql, args, err := dialect.Insert(usersTable).Prepared(true).Rows(
		goqu.Record{
			"name":          user.Name,
			"username":      user.Username,
			"password_hash": user.Password,
			"email":         user.Email,
			"got_job_at":    user.GotJobAt,
		},
	).Returning("id").ToSQL()

	if err != nil {
		logrus.Errorf("error during sql user register generation: %s", err.Error())
		return nil, err
	}

	row := r.db.QueryRow(sql, args...)

	if err := row.Scan(&user.Id); err != nil {
		logrus.Errorf("error during sql user register generation: %s", err.Error())
		return nil, err
	}

	return user, nil
}
