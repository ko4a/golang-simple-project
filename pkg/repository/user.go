package repository

import (
	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/postgres"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gorest/pkg/entity"
)

type UserRepository struct {
	db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{db: db}
}

func (repository *UserRepository) FindOneBy(field string, value interface{}) (*entity.User, error) {
	var result entity.User

	dialect := goqu.Dialect("postgres")

	sql, args, err := dialect.
		Select("id", "name", "username", "password_hash", "email").
		From("user_profile").
		Where(goqu.Ex{
			field: value,
		}).Limit(1).
		Prepared(true).ToSQL()

	if err != nil {
		logrus.Errorf("error during sql find by hash password username generation: %s", err.Error())
		return nil, err
	}

	row := repository.db.QueryRow(sql, args...)

	if err := row.Scan(&result.Id, &result.Name, &result.Username, &result.Password, &result.Email); err != nil {
		logrus.Errorf("error during sql execution FindByUsername: %s", err.Error())
		return nil, err
	}

	return &result, err
}
