package entity

import "time"

type Vacation struct {
	Id      int       `json:"id"`
	StartAt time.Time `json:"start_at"`
	EndAt   time.Time `json:"end_at"`
	UserId  int
}
