package entity

import "time"

type User struct {
	Id       int
	Name     string
	Username string
	Password string
	Email    string
	GotJobAt time.Time
}
